%define pointer_size 8

extern string_equals

global find_word

section .text

find_word:
    add rsi, pointer_size
    push rdi
    push rsi
    call string_equals
    pop rsi
    pop rdi

    sub rsi, pointer_size

    test rax, rax
    jnz .finish

    mov rsi, [rsi]
    test rsi, rsi
    jnz find_word

    xor rax, rax
    ret

.finish:
    mov rax, rsi
    ret
	