%include "words.inc"
%include "lib.inc"
extern find_word

%define buffer_size 256

global _start


section .data
	overflow_error: db "buffer overflow", 10, 0
    	not_found_key: db "key is not found", 10, 0
        buffer: times buffer_size db 0

section .text

_start:

    mov rdi,buffer
    mov rsi,buffer_size;
    call read_word
    cmp rax, 0
    je .size_error
    
    mov rdi,rax
    mov rsi, next
    push rdx
    call find_word
    pop rdx
    test rax,rax
    je .name_error

    mov rdi, rax
    add rdi, SHIFT
    add rdi, 0x1
    add rdi,rdx
    call print_string

    .finish:
	call print_newline
	call exit

    .size_error:
	mov rdi, size_error
	call error
	jmp .finish

    .name_error:
	mov rdi, name_error
	call error
	jmp .finish
