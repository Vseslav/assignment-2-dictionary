section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
 
exit: 
    mov rax, 60
    syscall


string_length:
    xor rax, rax
.lop:
    cmp byte [rdi+rax], 0
    je .return
    inc rax
    jmp .lop
.return:
    ret


print_string:
    mov rsi, rdi
    call string_length
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret


print_char:
    dec rsp
    mov [rsp], dil
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    inc rsp
    ret


print_newline:
    mov rdi, 0xA
    jmp print_char


print_uint:
    push r12 
    push r9
    mov r9, rsp
    mov byte[rsp], 0
    mov rax, rdi
    mov r12, 10 ; /10 -> 10 CC
    .loop:
        xor rdx, rdx 
        div r12
        add rdx, '0'
        dec rsp
        mov byte[rsp], dl
        cmp rax, 0
        je .end
        jmp .loop		        
    .end:
	mov rdi, rsp
    	call print_string
	mov rsp, r9
	pop r9
	pop r12
	ret


print_int:
    cmp rdi, 0
    jns .ns ;      
    .print_number:
        push rdi
        mov rdi, 0x2D
        call print_char
        pop rdi
        neg rdi
    .ns:
        call print_uint 
        ret


string_equals:
    .loop:
        mov al, byte [rdi]       
	cmp al, byte [rsi]     
	jne .not  
	inc rdi	   
	inc rsi
	cmp al, 0   
	jne .loop 
    .equals:
        mov rax, 1
	ret
    .not:
        xor rax, rax
	ret


 
read_char:
    dec rsp
    mov [rsp], byte 0
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    mov al, [rsp]
    inc rsp
    ret



read_word:
    push r12
    push r13
    push r14
    mov r12, rdi
    mov r13, rsi
    dec r13
    xor r14, r14
    .space_symbol:
        call read_char
        cmp rax, 0x20
        je .space_symbol
        cmp rax, 0x9
        je .space_symbol
        cmp rax, 0xA
        je .space_symbol
        cmp rax, 0
        je .end
								            
    .read:
   	cmp r13, r14
   	je .err
        mov byte[r12+r14], al
        inc r14
        call read_char
        cmp rax, 0
        je .end
        cmp rax, 0x20
        je .end
        cmp rax, 0x9
        je .end
        cmp rax, 0xA
        je .end
        jmp .read
								        
   .err:
       	xor rax, rax	
        pop r14
        pop r13
        pop r12
        ret
     
    .end:
	mov byte[r12+r14], 0
        mov rax, r12
	mov rdx, r14
        pop r14
        pop r13
        pop r12
        ret


parse_uint:
    xor rsi, rsi
    xor rax, rax

    push rbx
    mov rbx, 10;

.loop:    
    xor r10, r10
    mov r10b, byte [rdi+rsi]
    cmp r10, 0x30
    jl .return
    cmp r10, 0x39
    jg .return
    mul rbx
    add rax, r10
    sub rax, 0x30
    inc rsi
    jmp .loop
.return:
    pop rbx
    mov rdx, rsi
    ret



parse_int:
    xor rax, rax
    push rdi
    call read_char
    pop rdi
    cmp al, 0x2D
    jz .neg
    jmp parse_uint
   .neg: 
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret 


string_copy:
    call string_length
    cmp rax, rdx
    ja .notgood
    .loop:
        mov r10b, byte [rdi]
        mov byte [rsi], r10b
        inc rdi
        inc rsi
	test r10b, r10b
        jz .good
        jmp .loop
    .notgood:
        mov rax, 0
        ret
    .good:
        ret
